#coding: utf-8
"""
    What's Playing on Spotify?
"""

__author__  = "Ken V <ken@kverb.com>"
__version__ = "0.1"

import base64
import codecs
import json
import os.path
import re
import time

import tornado.escape as escape
import tornado.httpclient as client
import tornado.httpserver as httpserver
import tornado.ioloop as ioloop
import tornado.log  as log
import tornado.web  as web

from tornado.options import options

import config

from handlers import base, now_playing
from settings import settings

logger = log.app_log

print settings

class Application(web.Application):
    def __init__(self):
        handlers = [
            (r'/(?i)nowplaying', now_playing.NowPlayingHandler),
        ]

        web.Application.__init__(self, handlers, **settings)   

def main():

    app = Application()
    server = httpserver.HTTPServer(app)
    server.listen(options.port)
    try:
        ioloop.IOLoop.instance().start()
    except KeyboardInterrupt:
        ioloop.IOLoop.instance().stop()

if __name__ == '__main__':
    main()