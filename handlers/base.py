# base.py
import logging
import tornado.escape as escape
import tornado.httpclient as client
import tornado.web  as web
from tornado.options import options
from tornado.util import bytes_type, import_object, ObjectDict, raise_exc_info, unicode_type


# GLOBALS
logger = logging.getLogger(__name__)

class BaseHandler(web.RequestHandler):
    """Handler classes inherit from this base class. This is where 
    @WhatsApp.com domain names are checked."""

    def construct_client(self):

        if options.nocurl:
            # use simpleHTTP client 
            client.AsyncHTTPClient.configure(None, max_clients=10)
        else:
            # Use the curl-powered client by default
            client.AsyncHTTPClient.configure(
                'tornado.curl_httpclient.CurlAsyncHTTPClient', max_clients=40)
        self.async_client = client.AsyncHTTPClient()
        self.client = client.HTTPClient()
        self.headers = headers


    def gen_request(self, email=None, path=None, full_url=None, 
        method='GET', body=None) :
        """This generates a request object. Useful when an unknown number of 
        requests need to be made."""

        if path:
            path = path
            url  = BASE_URL + API_URL + path
        elif full_url:
            url = full_url
        logger.debug(url)
        if email:
            email=email
        else:
            email=self.email
            
        request = client.HTTPRequest(
            url = url,
            headers = headers,
            auth_mode = 'basic',
            auth_username = email + '/token', # Assumes use of API token.
            auth_password = API_TOKEN,
            validate_cert = False,
            method = method,
            body = body,
            )
        return request
