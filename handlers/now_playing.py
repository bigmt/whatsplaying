"""
The main "now playing handler file.
The app gets the current spotify track data via Applescript 
(using osascript on Popen).
"""

import logging
import tornado.web  as web

from subprocess import Popen, PIPE
from tornado.template import Loader
from base import BaseHandler

# Global templates

def get_current_track():
    osa = """
    if application "Spotify" is running then
        
        tell application "Spotify"
            
            set k_name to name of current track
            
        end tell
        
        
    else
        set k_name to ""
    end if
    return k_name
    """
    p = Popen(["osascript", "-e", osa], stdout=PIPE)
    out, err = p.communicate()
    return out.strip()

def get_current_artist():
    osa = """
    if application "Spotify" is running then
        tell application "Spotify"
            set k_artist to artist of current track
        end tell
    else
        set k_artist to ""
    end if
    return k_artist
    """
    p = Popen(["osascript", "-e", osa], stdout=PIPE)
    out, err = p.communicate()
    return out.strip()

def get_current_album():
    osa = """
    if application "Spotify" is running then
        tell application "Spotify"
            set k_album to album of current track
        end tell
    else
        set k_album to ""
    end if
    return k_album
    """
    p = Popen(["osascript", "-e", osa], stdout=PIPE)
    out, err = p.communicate()
    return out.strip()

def get_current_album_art():

    """WIP."""


class NowPlayingHandler(BaseHandler):
    """The now playing handler."""

    @web.asynchronous
    def get(self):
        track = get_current_track()
        print len(track)
        album = get_current_album()
        artist = get_current_artist()
        logging.debug("%s / %s / %s ", track, artist, album)
        if not track:
            self.render("nothing_playing.html")
        else:
            self.render("now_playing.html", track=track, album=album, artist=artist)
        self.finish()