# settings.py
import os
from tornado.options import define, options, parse_command_line

# GLOBAL OPTIONS
define("port", default=8888, 
    type=int, help="Port App should listen on. Default: 8888")
define("nocurl", default=False, 
    type=bool, help="If pycurl not installed, set this to true.")
parse_command_line()

settings = {
    'template_path' : os.path.join(os.path.dirname(__file__), "templates"),
    'static_path' : os.path.join(os.path.dirname(__file__), "static"),
    'cookie_secret' : 'merovingian',
    'debug' : True,
    'login_url': '/login',
}